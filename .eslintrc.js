module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    commonjs: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 12,
    sourceType: 'module',
  },
  extends: [
    'plugin:vue/recommended',
  ],
  // required to lint *.vue files
  plugins: ['vue'],
  // add your custom rules here
  rules: {
    semi: [2, 'always'],
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/no-v-html': 'off',
  },
  settings: {
    'import/core-modules': ['vue', 'vuex'],
  },
};
