export default {
  cluster: {
    minScale: 10,
    maxScale: 50,
    maxRadius: 180,
    point: {
      className: {
        wrapper: 'leaflet-cluster',
        group: 'leaflet__cluster',
        marker: 'leaflet__marker leaflet__marker--cluster',
        area: 'leaflet__area',
      },
      width: 40,
      height: 40,
    },
  },
  marker: {
    className: 'leaflet__marker leaflet__marker--point',
    iconUrl: '/img/map/marker.png',
    color: '#31C2A7',
    width: 60,
    height: 60,
    callbacks: {},
  },
  street: {
    color: '#31C2A7',
    weight: 6,
  },
  map: {
    imgUrl: '/img/map/map.svg',
    defaultWidth: 1928,
    defaultHeight: 1088,
  },
};

export const statusData = {
  addCard: 5,
  createPoint: 2,
  createLine: 3,
  createCard: 4,
  create: 1,
  passive: 0,
  remove: -1,
};

export const eventsData = {
  save: 0,
  reset: 1,
};
