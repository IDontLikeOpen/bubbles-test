import lodashGet from 'lodash/get';

export const format = {
  /**
   * @param phoneValue {string}
   * @returns {string}
   */
  phone: (phoneValue) => {
    const matchData = phoneValue.match(
      /(?<operator>[0-9]{3})(?<firstpart>[0-9]{3})(?<secondpart>[0-9]{2})(?<lastpart>[0-9]{2})/
    );

    if (!matchData) return '';

    const { groups } = matchData;
    const { operator, firstpart, secondpart, lastpart } = groups;

    return `+7 ${operator} ${firstpart} ${secondpart} ${lastpart}`;
  },
  /**
   *
   * @param number
   * @returns {string}
   */
  checkFloatNumber(number) {
    let splitValue = number.toString().split('.');
    let resultValue = number;

    if (splitValue.length) {
      resultValue = splitValue;
    }

    return resultValue;
  },
  /**
   * @param number {number}
   * @returns {string}
   */
  number(number) {
    if (!number) return '';

    let splitValue = number.toString().split('.');
    let floatPart = 0;
    let numberValue = 0;

    if (splitValue.length) {
      floatPart = splitValue[1] ? `.${splitValue[1]}` : '';
      numberValue = splitValue[0].toString();
    } else {
      numberValue = number.toString();
    }

    const formatNumber = numberValue.replace(
      /(?!^)(?=(?:\d{3})+(?:\.|$))/g,
      ' '
    );

    return `${formatNumber}${floatPart}`;
  },
};

export function isPirate(coinId) {
  return 'piratecash' === coinId;
}

export function sortItems(targetList, sortKey, order, pirateKey = 'id') {
  const list = targetList.slice();

  if (!sortKey) return list;

  return list.sort((a, b) => {
    const currentItem = lodashGet(a, sortKey) || 0;
    const nextItem = lodashGet(b, sortKey) || 0;
    const itemId = lodashGet(b, pirateKey);
    const isPirateItem = isPirate(itemId);

    let result = 0;

    if (isPirateItem || currentItem === nextItem) {
      result = 0;
    } else if (currentItem > nextItem) {
      result = 1;
    } else {
      result = -1;
    }

    return result * order;
  });
}

const baseFontSize = 16;

export const em = (pixels, context = baseFontSize) => pixels / context;

const iDevices = [
  'iPad Simulator',
  'iPhone Simulator',
  'iPod Simulator',
  'iPad',
  'iPhone',
  'iPod',
];
const userAgent =
  window.navigator.userAgent || window.navigator.vendor || window.opera;
const platform = window.navigator.platform;

export const isIPAD =
  /iPad/i.test(userAgent) ||
  /iPhone OS 3_1_2/i.test(userAgent) ||
  /iPhone OS 3_2_2/i.test(userAgent);

export const isIOS = !iDevices.indexOf(platform);

export const isIE =
  userAgent.indexOf('Edge/') > 0 ||
  userAgent.indexOf('Trident/') > 0 ||
  userAgent.indexOf('MSIE ') > 0;

export function isMobile() {
  let check = false;
  ((a) => {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(userAgent);
  return check || isIPAD;
}

export function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function getRandomFromArray(array) {
  return array[getRandomInt(0, array.length - 1)];
}

export function declension(num, expressions) {
  let result;
  let count = num % 100;
  if (count >= 5 && count <= 20) {
    result = expressions['2'];
  } else {
    count = count % 10;
    if (count === 1) {
      result = expressions['0'];
    } else if (count >= 2 && count <= 4) {
      result = expressions['1'];
    } else {
      result = expressions['2'];
    }
  }
  return result;
}

export function blobToFile(blob, fileName) {
  if (!this.isIE()) {
    return new File([blob], fileName, {
      type: blob.type,
      lastModified: Date.now(),
    });
  } else {
    let localBlob = blob;
    localBlob.lastModifiedDate = new Date();
    localBlob.name = fileName;
    return localBlob;
  }
}
