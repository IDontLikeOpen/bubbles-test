/* eslint-disable */
import { gsap } from "gsap";
import Bubble from "@/services/crypto-item";
import debounce from "lodash/debounce";
import lodashGet from "lodash/get";
import { cloneDeep } from "lodash/lang";
import { formatAmount, amountCompactText } from "@/services/format";

// resize of elements on window resize
// coords click

const bubblesSetupConfig = {
  multipliers: {
    radius: {
      min: 10,
      max: 80
    },
    resistance: 5
  },
  bubbles: {
    minSize: 10,
    simplifySize: 20,
    maxSize: 100,
    fillAreaSize: 50,
    coins: {
      negative: `coin-gray@3x.png`,
      positive: `coin-gold@3x.png`,
      specialNegative: `coin-special-gray@3x.png`,
      specialPositive: `coin-special@3x.png`
    }
  }
};

let image = new Image();
image.src = "https://i.ibb.co/6yNDmgm/coin-gray-3x.png";

let logo = new Image();
logo.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Playstation_logo_colour.svg/2560px-Playstation_logo_colour.svg.png";

const utils = {
  getRandomRadius(minRadius, maxRadius) {
    return Math.floor(Math.random() * (maxRadius - minRadius)) + minRadius;
  },
  animateRadiusChange({ item, endRadius }) {
    const initialConfig = { radius: item.radius };

    gsap.to(initialConfig, {
      duration: 5,
      radius: endRadius,
      repeat: 0,
      ease: "easeOut",
      onUpdate: () => {

        item.setRadius(initialConfig.radius);
      }
    });
  },
  isInsideCircle({ x, x0, y, y0, r }) {
    return Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0)) < r;
  },
  addEventListenerFromArray(callbackArray) {
    if (!callbackArray.length) return;

    callbackArray.forEach(({ eventName, target, callback }) => {
      target.addEventListener(eventName, (e) => callback?.(e));
    });
  },
  getRealCoords(event, canvas) {
    const {
      width: canvasWidth,
      top: canvasTop,
      left: canvasLeft
    } = canvas.getBoundingClientRect();

    const canvasCenter = { x: window.innerWidth / 2, y: window.innerHeight / 2 };

    const dx = canvasCenter.x - (window.innerWidth) / 2;
    const dy = canvasCenter.y - (window.innerHeight) / 2;
    // соотношение ширины по умолчанию к реальной ширине canvas
    const widthRatio = window.innerWidth / canvasWidth;
    // смещение стартовой точки отсчета для системы координат
    const offsetTop = window.pageYOffset + canvasTop;
    // Стартовые точки для X и Y
    const starPointX = event.pageX - canvasLeft;
    const startPointY = event.pageY - offsetTop;

    const x = (starPointX * widthRatio - dx);
    const y = (startPointY * widthRatio - dy);

    return { x, y };
  },
  isPirate(coinId) {
    return "piratecash" === coinId;
  },
  getCompactFormatValue(value) {
    const amountData = formatAmount(value);
    // let compactText = amountCompactText(value);
    let compactText = "some text";

    if (compactText) {
      compactText += " ";
    }

    return `${ amountData } ${ compactText }`;
  },
  getSquare(width, height) {
    return width * height;
  },
  randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  },
  getCircleSquare(radius) {
    return Math.PI * Math.pow(radius, 2);
  },
  getCircleRadiusBySquare(square) {
    const multiplier = Math.sign(square);

    return Math.sqrt((multiplier * square) / Math.PI);
  },
  calcRadiusIncreaseAmount({
                             radiusList,
                             containerSquare,
                             targetPercent = 30
                           }) {
    const itemsSquare = radiusList.reduce((acc, item) => {
      return acc + utils.getCircleSquare(item);
    }, 0);

    const inOnePercent = containerSquare / 100;
    const currentSquarePercents = (itemsSquare / containerSquare) * 100;
    const resultDifference = targetPercent - currentSquarePercents;
    const increaseByStep =
      (resultDifference * inOnePercent) / radiusList.length;
    const radiusByStep = utils.getCircleRadiusBySquare(increaseByStep);

    return radiusByStep / 2;
  }
};

export default class Bubbles {
  constructor(config) {
    this.config = {
      stage: {
        containerId: config.display.containerId,
        container: config.display.container,
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight - config.display.heightOffset,
        proportion: 1
      },
      imgPath: config.imgPath,
      coins: {
        initial: [], // initial bubbles data
        active: [], // active bubbles data
        callbacks: config.callbacks.coins,
        images: { // todo move to static config and map to add imgpath
          negative: `${ config.imgPath }coin-gray@3x.png`,
          positive: `${ config.imgPath }coin-gold@3x.png`,
          specialNegative: `${ config.imgPath }coin-special-gray@3x.png`,
          specialPositive: `${ config.imgPath }coin-special@3x.png`
        }
      }
    };

    this.userConfig = config.userConfig;
    this.selectedCurrency = config.currency;
    this.items = this.sortItems(config.data, "base.symbol");
    this.num = config.display.num;
    this.ctx = config.display.container.getContext('2d');

    console.log(this.ctx);

    this.stageCallbacks = [
      {
        eventName: "resize",
        target: window,
        callback: debounce(() => {
          this.recalculateBubbles();
        }, 500)
      },
      {
        eventName: "click",
        target: this.config.stage.container,
        callback: (e) => {
          if (!this.config.coins.callbacks?.click) return;

          const clickCoords = utils.getRealCoords(e, this.ctx.canvas);

          const resultBubbleData = this.detectBubble(clickCoords);

          resultBubbleData?.x && this.config.coins.callbacks?.click(resultBubbleData);
        }
      }
    ];
  }

  sortItems(list, sortKey) {
    return list.sort((a, b) => {
      const currentItem = lodashGet(a, sortKey) || 0;
      const nextItem = lodashGet(b, sortKey) || 0;

      let result = 0;

      if (currentItem === nextItem) {
        result = 0;
      } else if (currentItem > nextItem) {
        result = 1;
      } else {
        result = -1;
      }

      return result;
    });
  }

  mainLoop() {
    // this.ctx.canvas.width = window.innerWidth;
    // this.ctx.canvas.height = window.innerHeight;

    const { active } = this.config.coins;

    this.ctx.clearRect(0,0,this.config.stage.width,this.config.stage.height)

    for (let i = 0; i < active.length; i++) {
      active[i].update({
        width: this.config.stage.width,
        height: this.config.stage.height,
        bubbles: active.slice(0),
        start: i
      });
      active[i].drawBubble();
    }

    window.requestAnimationFrame(() => {
      this.mainLoop();
    });
  }

  // prepare canvas by config
  prepareCanvas() {
    const canvas = document.getElementById(this.config.stage.containerId);
    this.ctx = canvas.getContext("2d");

    this.ctx.canvas.width = this.config.stage.width;
    this.ctx.canvas.height = this.config.stage.height;
  }

  setInitialBubbleData() {
    this.config.coins.initial = cloneDeep(this.config.coins.active);
  }

  prepareBubbleData({ height, width }) {
    const { radius, resistance } = bubblesSetupConfig.multipliers;

    // get the smallest of canvas sides
    let minSize = Math.min(height, width);
    let resistanceLocal = minSize / resistance;

    // calculate velocities
    let velocityX = 0.3 + Math.floor(((0.5 - Math.random()) * resistanceLocal) / 100);
    let velocityY = 0.3 + Math.floor(((0.5 - Math.random()) * resistanceLocal) / 100);

    // get random radius
    let minRadius = minSize / radius.min;
    let maxRadius = minSize / radius.max;

    // get random position
    let radiusLocal = utils.getRandomRadius(minRadius, maxRadius);
    let x = Math.floor(Math.random() * (this.ctx.canvas.width - 2 * radiusLocal)) + radiusLocal;
    let y = Math.floor(Math.random() * (this.ctx.canvas.height - 2 * radiusLocal)) + radiusLocal;

    return { x, y, radius: radiusLocal, velocityX, velocityY, ctx: this.ctx };
  }

  // BUBBLES UTILS --- START

  getBubbleRadius(value, itemsValues) {
    const { minSize, maxSize } = bubblesSetupConfig.bubbles;
    const maxValue = Math.max(...itemsValues);
    const minValue = Math.min(...itemsValues);
    const bubbleResult = Math.round(
      (maxSize / (maxValue + Math.abs(minValue))) * Math.abs(value)
    );
    let resultSize = 0;

    if (bubbleResult < minSize) {
      resultSize = minSize;
    } else if (bubbleResult > maxSize) {
      resultSize = maxSize;
    } else {
      resultSize = bubbleResult;
    }

    return resultSize;
  }

  getPriceValueByPeriod(data) {
    const periodConfig = this.userConfig.period;
    const currency = this.selectedCurrency?.value;

    return data.changes?.price?.[periodConfig]?.[currency] || 0;
  }

  getBubbleValue({ data, currency }) {
    const { size, period } = this.userConfig;
    let result = null;

    switch (true) {
      case size === "performance":
        result = data.changes?.price?.[period]?.[currency] || 0;
        break;
      case size === "marketcap":
        result = data.market_cap?.[currency];
        break;
      case size === "volume":
      default:
        result = data.changes?.market_cap?.market_cap_24h?.[currency];
        break;
    }

    return result;
  }

  getBubbleContent({ data, currency }) {
    const { content, period } = this.userConfig;
    let result = null;

    switch (true) {
      case content === "price": {
        result = `${ utils.getCompactFormatValue(data.price?.[currency]) } ${
          this.selectedCurrency?.symbol
        }`;
        break;
      }
      case content === "name": {
        result = data.base?.name;
        break;
      }
      case content === "rank": {
        result = `# ${ data.ranks?.market_cap }`;
        break;
      }
      case content === "performance":
      default:
        result = `${ utils.getCompactFormatValue(
          data.changes?.price?.[period]?.[currency]
        ) }%`;
        break;
    }

    return result;
  }

  getBubbleCoin(priceValue, coinSlug) {
    const { positive, negative, specialPositive, specialNegative } =
      this.config.coins.images;
    let result = null;

    switch (true) {
      case utils.isPirate(coinSlug) &&
      this.userConfig.color === "performance" &&
      priceValue > 0:
        result = specialPositive;
        break;
      case utils.isPirate(coinSlug) &&
      this.userConfig.color === "performance" &&
      priceValue < 0:
        result = specialNegative;
        break;
      case utils.isPirate(coinSlug) && this.userConfig.color === "neutral":
        result = specialNegative;
        break;
      case this.userConfig.color === "performance" && priceValue > 0:
        result = positive;
        break;
      case this.userConfig.color === "performance" && priceValue < 0:
        result = negative;
        break;
      case this.userConfig.color === "neutral":
        result = negative;
        break;
      default:
        result = negative;
        break;
    }

    return result;
  }

  // BUBBLES UTILS --- END

  getRadiusConfig() {
    const itemsValues = this.items.map((item) => {
      return this.getBubbleValue({
        data: item,
        currency: this.selectedCurrency?.value
      });
    });

    const radiusList = itemsValues.map((item) => {
      return this.getBubbleRadius(item, itemsValues);
    });

    const stageSquare = utils.getSquare(
      this.config.stage.width,
      this.config.stage.height
    );
    const increaseAmount = utils.calcRadiusIncreaseAmount({
      radiusList,
      containerSquare: stageSquare,
      targetPercent: bubblesSetupConfig.bubbles.fillAreaSize
    });

    return {
      radiusList,
      increaseAmount
    };
  }

  drawCircles(itemsData) {
    const { radiusList, increaseAmount } = this.getRadiusConfig();
    const { height, width } = this.config.stage
    const { active } = this.config.coins;

    itemsData.forEach((item, index) => {
      const itemName = item.base.symbol;
      const priceValueByPeriod = this.getPriceValueByPeriod(item);

      const radius = radiusList[index] + increaseAmount;

      if (Number.isNaN(radius)) {
        return console.log("drawCircles isNaN");
      }

      const bubbleContent = this.getBubbleContent({
        data: item,
        currency: this.selectedCurrency?.value,
        config: this.userConfig
      });
      const coinImageUrl = this.getBubbleCoin(priceValueByPeriod, item.base.id);

      // get velocities
      const { resistance } = bubblesSetupConfig.multipliers;

      // get the smallest of canvas sides
      const minSize = Math.min(height, width);
      const resistanceLocal = minSize / resistance;

      // calculate velocities
      let velocityX = 0.3 + Math.floor(((0.5 - Math.random()) * resistanceLocal) / 100);
      let velocityY = 0.3 + Math.floor(((0.5 - Math.random()) * resistanceLocal) / 100);

      // get coords
      let x = utils.randomIntFromRange(radius, width - radius);
      let y = utils.randomIntFromRange(radius, height - radius);

      const imageUrl = utils.isPirate(item.base.id)
        ? `${ this.config.imgPath }pirate.svg`
        : item.images.large;

      // todo remove
      const coinImage = image
      const imageObj = logo

      // utils.createImage(coinImageUrl).then((coinImage) => {
      //   utils.createImage(imageUrl).then((imageObj) => {
      const bubbleConfig = {
        data: item,
        x,
        y,
        velocityX,
        velocityY,
        radius,
        coinImage,
        coinSlug: item.base.id,
        value: priceValueByPeriod,
        bubbleTitle: item?.base?.symbol?.toUpperCase(),
        bubbleContent,
        imageObj,
        index,
        container: this.config.stage.container
      };

      const bubble = new Bubble(bubbleConfig);
      active.push(bubble);
      //   });
      // });
    });
  }

  // fillBubbles() {
  //   const { height, width } = this.config.stage;
  //   const { active } = this.config.coins;
  //
  //   for (let i = 1; i <= this.num; i++) {
  //     const bubbleConfig = this.prepareBubbleData({ height, width });
  //     // create new bubble via prepared data
  //     const bubble = new Bubble(bubbleConfig);
  //     active.push(bubble);
  //   }
  // }

  getContainerProportion(newWidth) {
    return newWidth / this.config.stage.width;
  }

  updateProportion() {
    const width = this.config.stage.container.getBoundingClientRect().width

    this.config.stage.proportion = this.getContainerProportion(width);
  }

  recalculateBubbles () {
    const { active, initial } = this.config.coins;

    this.updateProportion();
    const { proportion } = this.config.stage;

    active.forEach((bubble, index) => {
      const endRadius = initial[index].radius * proportion;
      bubble.redrawBubble({ endRadius, bubble });
    });
  }

  detectBubble({ x, y }) {
    const { active } = this.config.coins;
    const target = active.find(bubble => utils.isInsideCircle({ x, x0: bubble.x, y, y0: bubble.y, r: bubble.radius }));

    if (target) return target;

    return {};
  }

  addListeners() {
    utils.addEventListenerFromArray(this.stageCallbacks);
  }

  init() {
    // this.prepareCanvas();
    this.drawCircles(this.items);
    this.setInitialBubbleData();
    this.addListeners();

    this.mainLoop();
  }
}
