/* eslint-disable */

import { gsap } from "gsap";

const baseConfig = {
  maxVelocity: 2,
  animationConfig: {
    duration: 2,
    repeat: 0,
    ease: "easeOut"
  },
  minSize: 10,
  simplifySize: 40,
  maxSize: 100,
  fillAreaSize: 50,
  images: {
    negative: `coin-gray@3x.png`,
    positive: `coin-gold@3x.png`,
    specialNegative: `coin-special-gray@3x.png`,
    specialPositive: `coin-special@3x.png`
  }
};

const textColor = {
  basic: "#484d51",
  piratePositive: "#ffd700",
  pirateNegative: "#c2c2c2"
};

const pirateConfig = {
  imageYOffsetMultiplier: -0.17,
  contentYOffsetMultiplier: -0.25,
  contentMultiplier: 0.8,
  radiusMultiplier: 1.28
};

const utils = {
  createImage(imgPath) {
    return new Promise((resolve) => {
      const imageObj = new Image();

      imageObj.onload = function() {
        resolve(imageObj);
      };

      imageObj.crossOrigin = "Anonymous";
      imageObj.src = imgPath;
    });
  },
  isPirate(coinId) {
    return "piratecash" === coinId;
  },
  rotate(v, theta) {
    return [
      v[0] * Math.cos(theta) - v[1] * Math.sin(theta),
      v[0] * Math.sin(theta) + v[1] * Math.cos(theta)
    ];
  },
  animateRadiusChange({ item, endRadius, duration = 3, callback }) {
    const initialConfig = { radius: item.radius };

    gsap.to(initialConfig, {
      duration,
      radius: endRadius,
      repeat: 0,
      ease: "easeOut",
      onUpdate: () => {
        callback(initialConfig.radius);
      }
    });
  }
};

let image = new Image();
image.src = "https://i.ibb.co/6yNDmgm/coin-gray-3x.png";

let logo = new Image();
logo.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Playstation_logo_colour.svg/2560px-Playstation_logo_colour.svg.png";

export default class Bubble {
  constructor({
                data, // mockup
                x,
                y,
                radius,
                coinImage,
                value,
                bubbleTitle,
                bubbleContent,
                imageObj,
                index,
                coinSlug,
                velocityX,
                velocityY,
                // ctx
                container
              }) {
    this.ctx = null;
    this.container = container;
    this.context = this.container.getContext("2d")
    this.width = this.container.getBoundingClientRect().width;
    this.height = this.container.getBoundingClientRect().height;

    this.x = x;
    this.y = y;
    this.radius = radius;
    this.index = index;
    this.coinSlug = coinSlug;
    this.velocityX = velocityX;
    this.velocityY = velocityY;
    this.coinImage = coinImage;
    this.value = value;
    this.bubbleTitle = bubbleTitle;
    this.bubbleContent = bubbleContent;
    this.imageObj = imageObj;
    this.coin = {};

    this.createBubble({
      coinImage,
      value,
      bubbleTitle,
      bubbleContent,
      imageObj,
      coinSlug
    });
  }

  createBubble() {
    const canvas = document.createElement("canvas");
    this.ctx = canvas.getContext("2d");

    this.ctx.canvas.id = this.coinSlug;
    this.setBubbleSize();

    this.ctx.beginPath();
    this.ctx.arc(this.radius, this.radius, this.radius, 0, Math.PI * 2);
    this.ctx.clip();

    this.coin = {
      circle: this.drawCircle(),
      logo: this.drawLogo(),
      title: this.drawTitle(),
      content: this.drawContent()
    };
    
    this.drawBubble()
  }

  drawBubble() {
    this.context.drawImage(
      this.ctx.canvas,
      this.x,
      this.y,
      this.radius,
      this.radius
    );
  }

  updateBubble() {
    this.coin.circle.draw();
    this.coin.title.draw();
    this.coin.content.draw();
    this.coin.logo.draw();
    
    // window.requestAnimationFrame(()=>this.updateBubble());
  }

  setBubbleSize() {
    this.ctx.canvas.width = this.radius * 2;
    this.ctx.canvas.height = this.radius * 2;
  }

  get canvas() {
    return this.ctx.canvas;
  }

  get bubbleMass() {
    let density = 0.1;

    return density * Math.PI * this.radius * this.radius;
  }

  get bubbleVelocity() {
    return [this.velocityX, this.velocityY];
  }

  setRadius(radius) {
    this.radius = radius;

    this.coin.circle.setRadius(this.radius);
    this.coin.title.setRadius(this.radius);
    this.coin.content.setRadius(this.radius);
    this.coin.logo.setRadius(this.radius);
  }

  getDistanceToBubble(other) {
    let dx = this.x - other.x;
    let dy = this.y - other.y;

    return Math.sqrt(dx * dx + dy * dy);
  }

  update({ bubbles, start }) {
    for (let i = start + 1; i < bubbles.length; i++) {
      let other = bubbles[i];

      if (this.getDistanceToBubble(other) < this.radius + other.radius) {
        //collison code
        let res = [this.velocityX - other.velocityX, this.velocityY - other.velocityY];
        if (res[0] * (other.x - this.x) + res[1] * (other.y - this.y) >= 0) {
          let m1 = this.bubbleMass;
          let m2 = other.bubbleMass;
          let theta = -Math.atan2(other.y - this.y, other.x - this.x);
          let v1 = utils.rotate(this.bubbleVelocity, theta);
          let v2 = utils.rotate(other.bubbleVelocity, theta);

          let u1 = utils.rotate(
            [
              (v1[0] * (m1 - m2)) / (m1 + m2) + (v2[0] * 2 * m2) / (m1 + m2),
              v1[1]
            ],
            -theta
          );
          let u2 = utils.rotate(
            [
              (v2[0] * (m2 - m1)) / (m1 + m2) + (v1[0] * 2 * m1) / (m1 + m2),
              v2[1]
            ],
            -theta
          );

          Math.abs(u1[0]) < baseConfig.maxVelocity && (this.velocityX = u1[0]);
          Math.abs(u1[1]) < baseConfig.maxVelocity && (this.velocityY = u1[1]);
          Math.abs(u2[0]) < baseConfig.maxVelocity && (other.velocityX = u2[0]);
          Math.abs(u2[1]) < baseConfig.maxVelocity && (other.velocityY = u2[1]);
        }
      }
    }

    if (this.x - this.radius <= 0) {
      this.x = this.radius;
    }

    if (this.x + this.radius >= this.width) {
      this.x = this.width - this.radius;
    }

    if (
      (this.x - this.radius <= 0 && this.velocityX < 0) ||
      (this.x + this.radius >= this.width && this.velocityX > 0)
    ) {
      this.velocityX = -this.velocityX;
    }

    if (this.y - this.radius <= 0) {
      this.y = this.radius;
    }

    if (this.y + this.radius >= this.height) {
      this.y = this.height - this.radius;
    }

    if (
      (this.y - this.radius <= 0 && this.velocityY < 0) ||
      (this.y + this.radius >= this.height && this.velocityY > 0)
    ) {
      this.velocityY = -this.velocityY;
    }

    //update velocities
    this.x += this.velocityX;
    this.y += this.velocityY;

    // this.setBubblePosition();
  }

  drawCircle() {
    return new CryptoCircle({
      coinImage: this.coinImage,
      x: this.radius,
      y: this.radius,
      radius: this.radius,
      ctx: this.ctx
    });
  }

  drawLogo() {
    return new CryptoLogo({
      simplifySize: baseConfig.simplifySize,
      value: this.value,
      imageObj: this.imageObj,
      coinSlug: this.coinSlug,
      ctx: this.ctx,
      x: this.radius,
      y: this.radius,
      // todo fix!
      radius: this.radius
    });
  }

  drawTitle() {
    return new CryptoTitle({
      simplifySize: baseConfig.simplifySize,
      radius: this.radius,
      text: this.bubbleTitle,
      coinSlug: this.coinSlug,
      value: this.value,
      x: this.radius,
      y: this.radius,
      ctx: this.ctx
    });
  }

  drawContent() {
    return new CryptoContent({
      simplifySize: baseConfig.simplifySize,
      radius: this.radius,
      text: this.bubbleContent,
      coinSlug: this.coinSlug,
      value: this.value,
      x: this.radius,
      y: this.radius,
      ctx: this.ctx
    });
  }

  getAnimatedConfig({ endRadius }) {
    return {
      ...baseConfig.animationConfig,
      radius: endRadius
    };
  }

  redrawBubble({ endRadius, bubble }) {
    this.coin.circle.update({ endRadius, bubble });
    this.coin.logo.update({ endRadius, bubble });
    this.coin.title.update({ endRadius, bubble });
    this.coin.content.update({ endRadius, bubble });
  }

  updateRadius({ endRadius, bubble }) {
    const initialConfig = { radius: this.radius };
    const animationSettings = this.getAnimatedConfig({ endRadius });

    // this.updateBubble()

    gsap.killTweensOf(initialConfig);
    gsap.to(initialConfig, {
      ...animationSettings,
      onUpdate: () => {
        this.setRadius(initialConfig.radius);
        // console.log(initialConfig.radius);
        // this.setBubbleSize();
        // this.redrawBubble({ endRadius: initialConfig.radius, bubble })
        // this.setBubblePosition()
      },
      onComplete:() => {
        // window.cancelAnimationFrame(this.updateBubble.bind(this))
      }
    });
  }
}

class CryptoCircle {
  constructor({ radius, coinImage, x, y, ctx }) {
    this.x = x;
    this.y = y;
    this.ctx = ctx;
    this.coinImage = coinImage;
    this.radius = radius;

    this.shadowConfig = {
      shadowColor: "rgba(2,15,31,0.6)",
      shadowOffsetX: 4,
      shadowOffsetY: 4,
      shadowBlur: 10
    };

    this.create();
  }

  getConfig() {
    return {
      image: this.coinImage,
      x: this.x - this.radius,
      y: this.y - this.radius,
      width: this.radius * 2,
      height: this.radius * 2
    };
  }

  getAnimatedConfig({ endRadius }) {
    return {
      ...baseConfig.animationConfig,
      radius: endRadius
    };
  }

  setRadius(radius) {
    this.radius = radius;
  }

  updateCoords({ x, y }) {
    this.x = x;
    this.y = y;
  }

  draw() {
    const { image, x, y, width, height } = this.getConfig();

    this.ctx.drawImage(image, x, y, width, height);

    // add shadow to coin
    Object.entries(this.shadowConfig).forEach(([key, value]) => {
      this.ctx[key] = value;
    });
  }

  create() {
    if (Number.isNaN(this.radius)) {
      return console.log("create isNaN CryptoCircle");
    }

    this.draw({
      x: this.x,
      y: this.y
    });
  }

  update({ endRadius, bubble }) {
    if (Number.isNaN(this.radius)) {
      return console.log("update isNaN CryptoCircle");
    }

    const initialConfig = { radius: this.radius };

    const animationSettings = this.getAnimatedConfig({ endRadius });

    gsap.killTweensOf(initialConfig);
    gsap.to(initialConfig, {
      ...animationSettings,
      onUpdate: () => {
        bubble.radius = initialConfig.radius;
        this.setRadius(initialConfig.radius);
      }
    });
  }
}

export class CryptoLogo {
  constructor({ simplifySize, radius, imageObj, value, coinSlug, x, y, ctx }) {
    this.item = null;
    this.simplifySize = simplifySize;
    this.coinSlug = coinSlug;
    this.image = imageObj;
    this.value = value;
    this.radius = this.getRadius(radius);

    this.x = x;
    this.y = y;
    this.ctx = ctx;

    this.create();
  }

  getRadius(radius) {
    return utils.isPirate(this.coinSlug)
      ? radius / 2 * pirateConfig.radiusMultiplier
      : radius / 2;
  }

  getPirateConfig() {
    return {
      offsetY: pirateConfig.imageYOffsetMultiplier * this.radius
    };
  }

  getCurrentConfig() {
    const config = {
      base: {
        image: this.image,
        x: this.x - this.radius / 2.4,
        y: this.y - this.radius / 1.3,
        width: this.radius / 1.2,
        height: this.radius / 1.2
      },
      simplify: {
        image: this.image,
        x: this.x - this.radius / 2,
        y: this.y - this.radius / 2,
        width: this.radius,
        height: this.radius
      }
    };

    const currentConfig = this.radius > this.simplifySize ? config.base : config.simplify;

    if (!utils.isPirate(this.coinSlug)) return currentConfig;

    return {
      ...currentConfig,
      ...this.getPirateConfig()
    };
  }

  getCircleConfig() {
    const config = {
      base: {
        radius: this.radius * 2
      },
      simplify: {
        radius: this.radius / 2
      }
    };

    const selectedConfig =
      this.radius > this.simplifySize ? config.base : config.simplify;

    return {
      fill: "#ffffff",
      ...selectedConfig
    };
  }

  updateCoords({ x, y }) {
    this.x = x;
    this.y = y;
  }

  draw() {
    const { image, x, y, width, height } = this.getCurrentConfig();

    this.ctx.drawImage(image, x, y, width, height);
  }

  create() {
    if (Number.isNaN(this.radius)) {
      return console.log("create isNaN CryptoLogo");
    }

    this.draw({
      x: this.x,
      y: this.y
    });
  }

  getAnimatedConfig({ endRadius }) {
    return {
      ...baseConfig.animationConfig,
      radius: endRadius
    };
  }

  setRadius(radius) {
    this.radius = radius;
  }

  update({ endRadius, priceValueByPeriod }) {
    if (Number.isNaN(this.radius)) {
      return console.log("update isNaN CryptoLogo");
    }

    priceValueByPeriod && (this.value = priceValueByPeriod);

    const initialConfig = { radius: this.radius };
    const animationSettings = this.getAnimatedConfig({ endRadius });

    gsap.killTweensOf(initialConfig);
    gsap.to(initialConfig, {
      ...animationSettings,
      onUpdate: () => {
        this.setRadius(initialConfig.radius);
      }
    });
  }
}

export class CryptoTitle {
  constructor({ radius, text, coinSlug, value, simplifySize, x, y, ctx }) {
    this.item = null;
    this.value = value;
    this.radius = radius;
    this.coinSlug = coinSlug;
    this.simplifySize = simplifySize;
    this.x = x;
    this.y = y;
    this.ctx = ctx;

    this.config = {
      text: text || "Name",
      fontStyle: "bold",
      fillText: utils.isPirate(coinSlug) ? "#ffffff" : "#484d51",
      align: "center"
    };

    if (radius < simplifySize) {
      this.config.fillText = "rgba(255,255,255,0)";
    }

    this.create();
  }

  getPirateConfig() {
    return {
      fillText: this.value < 0 ? textColor.pirateNegative : textColor.piratePositive,
      offsetY: pirateConfig.imageYOffsetMultiplier * this.radius
    };
  }

  getConfig() {
    const config = {
      x: this.x - this.radius / 5,
      y: this.y + this.radius / 5,
      width: this.radius,
      font: `${ this.config.fontStyle } ${ this.radius / 4 }px sans-serif`
    };

    if (!utils.isPirate(this.coinSlug)) return config;

    return {
      ...config,
      ...this.getPirateConfig()
    };
  }

  updateCoords({ x, y }) {
    this.x = x;
    this.y = y;
  }

  draw() {
    const { font, x, y, width } = this.getConfig();

    this.ctx.width = width;
    this.ctx.textAlign = this.config.align;
    this.ctx.font = font;
    this.ctx.fillText(this.config.text, x, y);
  }

  create() {
    if (Number.isNaN(this.radius)) {
      return console.log("create isNaN CryptoTitle");
    }

    this.draw({
      x: this.x,
      y: this.y
    });
  }

  getAnimatedConfig({ endRadius }) {
    return {
      ...baseConfig.animationConfig,
      radius: endRadius
    };
  }

  setRadius(radius) {
    this.radius = radius;
  }

  update({ endRadius, priceValueByPeriod }) {
    if (Number.isNaN(this.radius)) {
      return console.log("update isNaN CryptoTitle");
    }

    priceValueByPeriod && (this.value = priceValueByPeriod);

    const initialConfig = { radius: this.radius };
    const animationSettings = this.getAnimatedConfig({ endRadius });

    gsap.killTweensOf(initialConfig);
    gsap.to(initialConfig, {
      ...animationSettings,
      onUpdate: () => {
        this.setRadius(initialConfig.radius);
      }
    });
  }
}

export class CryptoContent {
  constructor({ text, radius, coinSlug, simplifySize, value, x, y, ctx }) {
    this.item = null;
    this.radius = radius;
    this.value = value;
    this.simplifySize = simplifySize;
    this.text = text;
    this.coinSlug = coinSlug;
    this.radius = this.getRadius(radius);
    this.x = x;
    this.y = y;
    this.ctx = ctx;

    this.config = {
      text: text || "Нет данных",
      fontStyle: "bold",
      fillText: utils.isPirate(coinSlug) ? "#ffffff" : "#484d51",
      align: "center"
    };

    if (radius < simplifySize) {
      this.config.fillText = "rgba(255,255,255,0)";
    }

    this.create();
  }

  getRadius(radius) {
    return utils.isPirate(this.coinSlug)
      ? radius * pirateConfig.contentMultiplier
      : radius;
  }

  getPirateConfig() {
    return {
      fillText:
        this.value < 0 ? textColor.pirateNegative : textColor.piratePositive,
      offsetY: pirateConfig.contentYOffsetMultiplier * this.radius
    };
  }

  getConfig() {
    const valueLength = this.text.length;

    const config = {
      x: this.x,
      y: this.y + this.radius / 2,
      font: `${ this.config.fontStyle } ${ this.getSizeByContentLength(valueLength) }px sans-serif`,
      width: this.radius
    };

    if (!utils.isPirate(this.coinSlug)) return config;

    return {
      ...config,
      ...this.getPirateConfig()
    };
  }

  getSizeByContentLength(contentLength) {
    const minTextLength = 6;
    let delimited =
      contentLength < minTextLength ? minTextLength : contentLength;

    return (this.radius * 1.5) / delimited; // тут магия цифр
  }

  updateCoords({ x, y }) {
    this.x = x;
    this.y = y;
  }

  draw() {
    const { font, x, y, width } = this.getConfig();

    this.ctx.width = width;
    this.ctx.textAlign = this.config.align;
    this.ctx.font = font;
    this.ctx.fillText(this.config.text, x, y);
  }

  create() {
    if (Number.isNaN(this.radius)) {
      return console.log("create isNaN CryptoContent");
    }

    this.draw({
      x: this.x,
      y: this.y
    });
  }

  getAnimatedConfig({ endRadius }) {
    return {
      ...baseConfig.animationConfig,
      radius: endRadius
    };
  }

  setRadius(radius) {
    this.radius = radius;
  }

  update({ endRadius, text, priceValueByPeriod }) {
    if (Number.isNaN(this.radius)) {
      return console.log("update isNaN CryptoContent");
    }

    text && (this.text = text);
    priceValueByPeriod && (this.value = priceValueByPeriod);

    const initialConfig = { radius: this.radius };
    const animationSettings = this.getAnimatedConfig({ endRadius });

    gsap.killTweensOf(initialConfig);
    gsap.to(initialConfig, {
      ...animationSettings,
      onUpdate: () => {
        this.setRadius(initialConfig.radius);
      }
    });
  }
}
