import { format } from '@/tools/Helpers';
// import i18n from '@/plugins/i18n';

export function formatToPercent(value = 0) {
  const toFixedValue = value > 100 || value === 0 ? 0 : 1;
  const fixedValue = value.toFixed(toFixedValue);

  return `${fixedValue} %`;
}

export function formatAmount(value) {
  if (!value) return 0;

  const minPartsLength = 3;
  const formatNumber = format.number(value);
  const parts = formatNumber.split(' ');
  const toFixedValue = getFixedValue(value);
  let resultValue = null;
  const fixedValue = value < 100 ? 2 : 0; // TODO: need fix code style

  if (parts.length < minPartsLength) {
    resultValue = parseFloat(value).toFixed(toFixedValue || fixedValue);

    return format.number(resultValue);
  }

  const pow = Math.pow(1000, parts.length - 1);

  resultValue = value / pow;

  if (toFixedValue) {
    resultValue = resultValue.toFixed(toFixedValue || fixedValue);
  } else {
    resultValue = parseInt(resultValue, 10);
  }

  const scrubPart = parts[1].slice(0, 2);

  return `${resultValue},${scrubPart}`;
}

export function amountCompactText(value) {
  if (!value) return '';

  const dictionary = [
    // `${i18n.t('format.million')}.`,
    // `${i18n.t('format.billion')}.`,
    // `${i18n.t('format.trillion')}.`,
  ];
  const parts = format.number(value).split(' ');

  if (parts.length < 3) return '';

  return dictionary[parts.length - 3] || '';
}

export function getFixedValue(value) {
  let afterComma = 2;

  if (value > 0) {
    afterComma = 3 - Math.ceil(Math.log10(value));
  }

  switch (true) {
    case value < 0 || afterComma < 0:
      afterComma = 0;
      break;
    case afterComma > 10:
      afterComma = 10;
      break;
    case value > 1e6 || afterComma === 1:
      afterComma = 2;
      break;

    default:
      // console.log('Не вошел ни в один case:', afterComma);
      break;
  }

  return afterComma;
}

export function getModifierByGrowth(value) {
  const modifiersDictionary = {
    positive: 'positive',
    negative: 'negative',
    default: 'stable',
  };
  let modifier = '';

  switch (true) {
    case value > 0:
      modifier = modifiersDictionary.positive;
      break;
    case value < 0:
      modifier = modifiersDictionary.negative;
      break;
    default:
      modifier = modifiersDictionary.default;
      break;
  }

  return modifier;
}

export default {
  formatToPercent,
  formatAmount,
  amountCompactText,
  getFixedValue,
  getModifierByGrowth,
};
