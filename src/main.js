import Vue from 'vue';
import App from './App.vue';
import router from './router';

import axios from 'axios';

import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

import Typograf from 'typograf';

let Tp = new Typograf({ locale: ['ru', 'en-US'] });

Vue.directive('typograph', {
  bind: (el) => {
    if (process.browser) {
      let element = el;
      element.innerHTML = Tp.execute(el.innerHTML);
    }
  },
});

import '@/assets/scss/app.scss';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
